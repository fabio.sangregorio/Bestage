class ApplicationMailer < ActionMailer::Base
  default from: "me@db82fef59e129939a91c1c11fc5ca54b.com"
  layout 'mailer'
end
