$(document).ready ->
  # Initializes jQuery datepicker
  $('#student_birth_date').datepicker
    changeMonth: true
    changeYear: true
    yearRange: '1900:<%= Date.today.year %>'
    minDate: '-100Y'
    maxDate: '-1D'
    dateFormat: 'dd/mm/yy'

  # Adds a skill to the skills list and removes it from the select.
  $('#add_skill').click ->
    selected_skill = $('#student_skills option:selected')
    selected_level = $('#skill_level').val()
    d = new Date();
    t = d.getTime();
    name = 'student[student_skills_attributes]['+ t + ']'

    if selected_level < 1 or selected_level > 5
      $('#skill_level').css 'border-color', 'red'
      return
    else
      $('#skill_level').css 'border-color', ''

    $('#skills-list').append '<li class=\'list-group-item\'>' +
        # Skill text: [0]
        '<div class=\'skill-entry\'>' + selected_skill[0].innerText + '</div>' +
        # Skill level: [1]
        '<input class=\'skill-level form-control\' type=\'number\'
                value=\''+ selected_level + '\' ' +
                'name=\'' + name + '[level]\' />' +
        # Skill destroy: [2]
        '<input type=\'hidden\' name=\'' + name + '[_destroy]\'' +
                'value=\'false\' />' +
        # Remove button: [3]
        '<div class=\'btn btn-danger remove_skill\'>-</div>' +
      '</li>' +
      # Skill id
      '<input type=\'hidden\' name=\'' + name + '[skill_id]\'' +
              'value=\'' + selected_skill[0].value + '\' data-temp=\'true\' />'

    $('#student_skills option[value=\'' +
                 selected_skill[0].value + '\']').remove()
    $('#skill_level').val '1'
    return

  # Removes a skill from the skills list and adds it back to the select.
  $('body').on 'click', '.remove_skill', (event) ->
    parent   = $(this).parent()
    children = $(this).parent().children()
    skill    = children[0]
    id       = $(parent).next()
    destroy  = children[2]
    destroy.value = 'true'

    $('#student_skills').append '<option value=\'' + id[0].value +
                                        '\'>' + skill.innerText + '</option>'
    sortSelect '#student_skills'

    if id.data('temp') != true
      $(parent).after id
      $(parent).after destroy
    else
      $(id).remove()

    $(parent).remove()
    return

  genPw = ->
    length = 10
    charset = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'
    retVal = ''
    i = 0
    n = charset.length
    while i < length
      retVal += charset.charAt(Math.floor(Math.random() * n))
      ++i
    $('#student_password').val retVal
    $('#student_password_confirmation').val retVal
    return

  $('#genpw-btn').click genPw

  $("#search").autocomplete
    source: "/students_load_suggestions"

  $("#goStudent").click ->
    window.location.href = "/students_redirect?name=" + $("#search").val()

  $("#close-hint").click ->
    $("#skill-hint-overlay").hide()

  $("#show-hint").click ->
    $("#skill-hint-overlay").show()

  return
