module ApplicationHelper

  # Returns the full title on a per-page basis.
  def full_title(page_title = "")
    base_title = "Bestage"
    if page_title.empty?
      base_title
    else
      "#{page_title} | #{base_title}"
    end
  end

  # Returns true if a given email exists in any model.
  def does_alredy_exist?(email)
    !School.find_by(email: email).nil? || !Student.find_by(email: email).nil? ||
                                          !Company.find_by(email: email).nil?
  end

  # Finds the user associated with the given email in any model, if any.
  def find_user_by_email(email)
    if does_alredy_exist? email
      user = School.find_by(email: email)
      unless user.nil?
        return user
      end
      user = Student.find_by(email: email)
      unless user.nil?
        return user
      end
      return user = Company.find_by(email: email)
    end
  end

  # Finds the user by id across classes, if any.
  def find_user_by_id(id, user_class)
    if user_class == School.to_s
      School.find_by(id: id)
    elsif user_class == Student.to_s
      Student.find_by(id: id)
    elsif user_class == Company.to_s
      Company.find_by(id: id)
    else
      nil
    end
  end

  # Converts the class of the user in symbol.
  def user_to_sym(user)
    user.class.to_s.downcase.to_sym
  end

end
