class Student < ActiveRecord::Base
  include MatchesModule

  attr_accessor :remember_token, :activation_token, :reset_token

  has_many :student_skills, inverse_of: :student
  has_many :skills, through: :student_skills
  has_many :matches
  has_one  :stage

  accepts_nested_attributes_for :student_skills, allow_destroy: true

  before_create :create_activation_digest
  before_save :downcase_email

  validates :name,        presence: true, length: { maximum: 50 }
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i
  validates :email,       presence: true, length: { maximum: 255 },
                          format: { with: VALID_EMAIL_REGEX },
                          uniqueness: { case_sensitive: false }
  validates :birth_date,  presence: true
  validate  :birth_date_cannot_be_in_the_future
  validates :school_year, presence: true
  validate  :first_letter_must_be_a_school_year
  has_secure_password
  validates :password, length: { minimum: 6 }, allow_blank: true

  # Returns the hash digest of the given string.
  def Student.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                  BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end

  # Returns a random token.
  def Student.new_token
    SecureRandom.urlsafe_base64
  end

  # Returns true if the given token matches the digest.
  def authenticated?(attribute, token)
    digest = send("#{attribute}_digest")
    return false if digest.nil?
    BCrypt::Password.new(digest).is_password?(token)
  end

  # Remembers a student in the database for use in persistent sessions.
  def remember
    self.remember_token = Student.new_token
    update_attribute(:remember_digest, Student.digest(remember_token))
  end

  # Forgets a student
  def forget
    update_attribute(:remember_digest, nil)
  end

  # Activates an account.
  def activate
    update_attribute(:activated,    true)
    update_attribute(:activated_at, Time.zone.now)
  end

  def send_activation_email
    UserMailer.account_activation(self).deliver_now
  end

  # Sets the password reset attributes.
  def create_reset_digest
    self.reset_token = Student.new_token
    update_attribute(:reset_digest,  Student.digest(reset_token))
    update_attribute(:reset_sent_at, Time.zone.now)
  end

  # Sends password reset email.
  def send_password_reset_email
    UserMailer.password_reset(self).deliver_now
  end

  # Returns true if a password reset has expired.
  def password_reset_expired?
    reset_sent_at < 2.hours.ago
  end

  # Assigns a skill to the student.
  def assign_skill(skill, level)
    StudentSkill.create(student_id: self.id, skill_id: skill.id, level: level)
  end

  # Removes a skill from the student.
  def remove_skill(skill)
    StudentSkill.find_by(skill_id: skill.id).destroy
  end

  # Returns true if the student has the skill.
  def has_skill?(skill)
    !StudentSkill.find_by(skill_id: skill.id).nil?
  end

  def find_matches
    start = Time.now
    matches = []
    existing_matches = Match.all.where(student_id: self.id)
    Company.eager_load(:company_skills).all.each {|company| matches.push find_match(self, company,
                                                        self.student_skills,
                                                        company.company_skills,
                                                        existing_matches)}
    puts "Matches: #{matches.reject(&:blank?)}".green
    puts "Execution time: #{Time.now - start} seconds".brown
    puts "Total matches in db: #{Match.all.length}"
  end

  def self.terms_for(prefix)
    suggestions = where("name like ?", "#{prefix}_%")
    suggestions.limit(10).pluck(:name)
  end

  private

    # Returns an error if the birth date is in the future.
    def birth_date_cannot_be_in_the_future
      if birth_date.nil? || birth_date > Date.today
        errors.add(:birth_date, "can't be in the future")
      end
    end

    def first_letter_must_be_a_school_year
      initial = school_year[0,1].to_i
      second = school_year[1,2]
      if initial < 1 || initial > 5 || second.to_i > 0
        errors.add(:school_year, "must be number + letter")
      end
    end

    # Converts email to all lower-case.
    def downcase_email
      self.email = email.downcase
    end

    # Creates and assigns the activation token and digest.
    def create_activation_digest
      self.activation_token = Student.new_token
      self.activation_digest = Student.digest(activation_token)
    end

end
