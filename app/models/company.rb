class Company < ActiveRecord::Base
  include MatchesModule

  attr_accessor :remember_token, :activation_token, :reset_token

  has_many :company_skills, inverse_of: :company
  has_many :skills, through: :company_skills
  has_many :stages
  has_many :matches

  accepts_nested_attributes_for :company_skills, allow_destroy: true

  before_create :create_activation_digest
  before_save :downcase_email
  before_save :save_match_value

  validates :name, presence: true, length: { maximum: 50 }
  validates :email, presence: true, length: { maximum: 255 }
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i
  validates :email,       presence: true, length: { maximum: 255 },
                          format: { with: VALID_EMAIL_REGEX },
                          uniqueness: { case_sensitive: false }
  validates :description, presence: true
  validates :address,     presence: true
  validates :vacancies,   presence: true
  validates_numericality_of :vacancies, greater_than: -1
  validates :match_value, :inclusion => { :in => -5..5 }, allow_blank: true
  has_secure_password
  validates :password, length: { minimum: 6 }, allow_blank: true

  # Returns the hash digest of the given string.
  def Company.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                  BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end

  # Returns a random token.
  def Company.new_token
    SecureRandom.urlsafe_base64
  end

  # Returns true if the given token matches the digest.
  def authenticated?(attribute, token)
    digest = send("#{attribute}_digest")
    return false if digest.nil?
    BCrypt::Password.new(digest).is_password?(token)
  end

  # Remembers a student in the database for use in persistent sessions.
  def remember
    self.remember_token = Company.new_token
    update_attribute(:remember_digest, Company.digest(remember_token))
  end

  # Forgets a student
  def forget
    update_attribute(:remember_digest, nil)
  end

  # Activates an account.
  def activate
    update_attribute(:activated,    true)
    update_attribute(:activated_at, Time.zone.now)
  end

  def send_activation_email
    UserMailer.account_activation(self).deliver_now
  end

  # Sets the password reset attributes.
  def create_reset_digest
    self.reset_token = Company.new_token
    update_attribute(:reset_digest,  Company.digest(reset_token))
    update_attribute(:reset_sent_at, Time.zone.now)
  end

  # Sends password reset email.
  def send_password_reset_email
    UserMailer.password_reset(self).deliver_now
  end

  # Returns ture if a password reset has expired.
  def password_reset_expired?
    reset_sent_at < 2.hours.ago
  end

  # Assigns a skill to the company.
  def assign_skill(skill, level)
    CompanySkill.create(company_id: self.id, skill_id: skill.id, level: level)
  end

  # Removes a skill from the company.
  def remove_skill(skill)
    CompanySkill.find_by(skill_id: skill.id).destroy
  end

  # Returns true if the company has the skill.
  def has_skill?(skill)
    !CompanySkill.find_by(skill_id: skill.id).nil?
  end

  # Calculates the Company's value based on the skills level for the match
  def calculate_match_value
    skills = self.company_skills.map{|s| s.level}
    skills.map{|s| s**2}.inject(0, :+) / skills.inject(0, :+).to_f
  end

  # BEFORE SAVE: Saves the match value after updating the company's skills
  def save_match_value
    changed = company_skills.any?{ |c| c.new_record? || c.marked_for_destruction?}
    if changed
      self.match_value = calculate_match_value
      find_matches
    end
  end

  # Finds and creates the matches between the company and all the students.
  def find_matches
    start = Time.now
    matches = []
    existing_matches = Match.all.where(company_id: self.id)
    Student.eager_load(:student_skills).all.each {|student| matches.push find_match(student, self,
                                                        student.student_skills,
                                                        self.company_skills,
                                                        existing_matches)}
    puts "Matches: #{matches.reject(&:blank?)}".green
    puts "Execution time: #{Time.now - start} seconds".brown
    puts "Total matches in db: #{Match.all.length}"
  end

  private

    # Converts email to all lower-case.
    def downcase_email
      self.email = email.downcase
    end

    # Creates and assigns the activation token and digest.
    def create_activation_digest
      self.activation_token = Company.new_token
      self.activation_digest = Company.digest(activation_token)
    end

end
