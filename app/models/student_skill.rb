class StudentSkill < ActiveRecord::Base
  belongs_to :student, inverse_of: :student_skills
  belongs_to :skill
  validates_presence_of :student
  validates_presence_of :skill
  validates :skill_id, uniqueness: { scope: :student_id }

  validates :level, presence: true, :inclusion => { :in => 1..5 }
end
