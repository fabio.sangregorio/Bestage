class CompanySkill < ActiveRecord::Base
  belongs_to :company, inverse_of: :company_skills
  belongs_to :skill
  validates_presence_of :company
  validates_presence_of :skill
  validates :skill_id, uniqueness: { scope: :company_id }

  validates             :level, presence: true, :inclusion => { :in => 1..5 }
end
