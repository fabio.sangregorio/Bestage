class Skill < ActiveRecord::Base
  has_many :student_skills, dependent: :destroy
  has_many :students, through: :student_skills
  has_many :company_skills, dependent: :destroy
  has_many :companies, through: :company_skills
  validates :name, presence: true, uniqueness: { case_sensitive: false }
  validates :school_year, presence: true, :inclusion => { :in => 1..5 }
  validates :course, presence: true
end
