class Stage < ActiveRecord::Base
  belongs_to :student
  belongs_to :company
  validates_presence_of :student
  validates_presence_of :company
  validate :end_time_is_greater_than_start_time
  validates :student_id, uniqueness: { scope: :company_id }

  validates :start_time, presence: true
  validates :end_time,   presence: true

  private

    def end_time_is_greater_than_start_time
      if end_time < start_time
        errors.add(:start_time, "non può essere maggiore della data di fine")
      end
    end

end
