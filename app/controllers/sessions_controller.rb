class SessionsController < ApplicationController
  include ApplicationHelper
  before_action :not_logged_in_user, only: [:new, :create]

  def new
  end

  def create
    user = find_user_by_email(params[:session][:email].downcase)
    if user && user.authenticate(params[:session][:password])
      if user.activated?
        log_in user
        params[:session][:remember_me] == '1' ? remember(user) : forget(user)
        redirect_back_or user
      else
        message = "Account not activated. "
        message += "Check your email for the activation link."
        flash[:warning] = message
        redirect_to root_url
      end
    else
      flash.now[:danger] = "Combinazione email/password non valida"
      render "new"
    end
  end

  def destroy
    log_out if logged_in?
    redirect_to root_url
  end

  private

    # Redirects the user to the root url if alredy logged in.
    def not_logged_in_user
      if logged_in?
        flash[:warning] = "Sei già loggato."
        redirect_to root_url
      end
    end

end
