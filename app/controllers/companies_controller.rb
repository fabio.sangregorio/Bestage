class CompaniesController < ApplicationController
  include ApplicationHelper, MatchesModule
  before_action :logged_in_user,   only: [:index, :edit, :update, :destroy]
  before_action :correct_user,     only: [:edit, :update]
  before_action :admin_user,       only: [:destroy]

  def index
    @companies = Company.paginate(page: params[:page])
  end

  def show
    @company = Company.find(params[:id])
  end

  def new
    @company = Company.new
  end

  def create
    @company = Company.new(company_params)
    if does_alredy_exist? @company.email
      flash.now[:danger] = "L'email è già stata presa."
      render "new"
    elsif @company.save
      @company.send_activation_email
      flash[:info] = "Per favore controlla la tua email per attivare l'account."
      redirect_to root_url
    else
      render 'new'
    end
  end

  def edit
    @company = Company.find(params[:id])
  end

  def update
    @company = Company.find(params[:id])
    if @company.update_attributes(company_params)
      flash[:success] = "Profilo aggiornato"
      redirect_to @company
    else
      render "edit"
    end
  end

  def destroy
    Company.find(params[:id]).destroy
    flash[:success] = "Azienda eliminata"
    redirect_to companies_url
  end

  private

    def company_params
      params.require(:company).permit(:name, :email, :description, :address,
                          :vacancies, :password, :password_confirmation,
                          company_skills_attributes: [:id, :skill_id, :level,
                                                      :_destroy] )
    end

    # Before filters

    # Confirms a logged-in user.
    def logged_in_user
      unless logged_in?
        store_location
        flash[:danger] = "Devi essere loggato per eseguire quest'azione."
        redirect_to login_url
      end
    end

    # Confirms the correct user (the student or the school).
    def correct_user
      @company = Company.find(params[:id])
      redirect_to(root_url) unless @company == current_user
    end

    # Confirms an admin user (the school)
    def admin_user
      unless (current_user.is_a? School) && current_user.admin?
        flash[:danger] = "Devi essere loggato come admin
                          per eseguire quest'azione."
        redirect_to root_url
      end
    end

end
