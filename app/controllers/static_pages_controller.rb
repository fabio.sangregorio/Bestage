class StaticPagesController < ApplicationController
  def home
    companies   = Company.all.eager_load(:company_skills)
    students    = Student.all.eager_load(:student_skills)
    s_averages    = []
    students.each{|s| s_averages.push(s.student_skills.map{|ss| ss.level}.
                                      inject(:+)/s.student_skills.length.to_f)}
    avgsskills   = s_averages.inject(:+) / s_averages.length

    c_averages    = []
    companies.each{|c| c_averages.push(c.company_skills.map{|cs| cs.level}.
                                      inject(:+)/c.company_skills.length.to_f)}
    avgcskills   = c_averages.inject(:+) / c_averages.length

    @stats = { ncompanies: companies.length,
               nstudents:  students.length,
               nskills:    Skill.all.length,
               nstages:    Stage.all.length,
               avgsskills: avgsskills.round(2),
               avgcskills: avgcskills.round(2)
             }
  end

  def stats
    companies   = Company.all.eager_load(:company_skills)
    students    = Student.all.eager_load(:student_skills)
    s_averages    = []
    students.each{|s| s_averages.push(s.student_skills.map{|ss| ss.level}.
                                      inject(:+)/s.student_skills.length.to_f)}
    avgsskills   = s_averages.inject(:+) / s_averages.length

    c_averages    = []
    companies.each{|c| c_averages.push(c.company_skills.map{|cs| cs.level}.
                                      inject(:+)/c.company_skills.length.to_f)}
    avgcskills   = c_averages.inject(:+) / c_averages.length

    @stats = { ncompanies: companies.length,
               nstudents:  students.length,
               nskills:    Skill.all.length,
               nstages:    Stage.all.length,
               avgsskills: avgsskills.round(2),
               avgcskills: avgcskills.round(2)
             }
  end

  def about
  end
end
