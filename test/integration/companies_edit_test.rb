require 'test_helper'

class CompaniesEditTest < ActionDispatch::IntegrationTest

  def setup
    @company = companies(:abb)
  end

  test "unsuccessful edit" do
    log_in_as(@company)
    get edit_company_path(@company)
    assert_template "companies/edit"
    patch company_path(@company), company: { name:        "",
                                             email:       "azienda@esempiocom",
                                             description: "Descrizione esempio",
                                             address:     "Indirizzo esempio",
                                             vacancies:   -1,
                                             password:             "baz",
                                             password_confirmation:"foobar" }
    assert_template "companies/edit"
  end

  test "successful edit" do
    log_in_as(@company)
    get edit_company_path(@company)
    assert_template 'companies/edit'
    name = "Foo Bar"
    email = "foo@bar.com"
    patch company_path(@company), company: { name:        name,
                                             email:       email,
                                             description: "Descrizione esempio",
                                             address:     "Indirizzo esempio",
                                             vacancies:   1,
                                             password:             "foobar",
                                             password_confirmation:"foobar" }
    assert_not flash.empty?
    assert_redirected_to @company
    @company.reload
    assert_equal @company.name, name
    assert_equal @company.email, email
  end

  test "successful edit with friendly forwarding" do
    get edit_company_path(@company)
    log_in_as(@company)
    assert_redirected_to edit_company_path(@company)
    name = "Foo Bar"
    email = "foo@bar.com"
    patch company_path(@company), company: { name:        name,
                                             email:       email,
                                             description: "Descrizione esempio",
                                             address:     "Indirizzo esempio",
                                             vacancies:   1,
                                             password:             "foobar",
                                             password_confirmation:"foobar" }
    assert_not flash.empty?
    assert_redirected_to @company
    @company.reload
    assert_equal @company.name, name
    assert_equal @company.email, email
  end

end
