require 'test_helper'

class StudentsEditTest < ActionDispatch::IntegrationTest

  def setup
    @student = students(:archer)
  end

  test "unsuccessful edit" do
    log_in_as(@student)
    get edit_student_path(@student)
    assert_template "students/edit"
    patch student_path(@student), student: { name:  "",
                                             email: "foo@invalid",
                                             password:              "foo",
                                             password_confirmation: "bar" }
    assert_template "students/edit"
  end

  test "successful edit" do
    log_in_as(@student)
    get edit_student_path(@student)
    assert_template 'students/edit'
    name = "Foo Bar"
    email = "foo@bar.com"
    patch student_path(@student), student: { name: name,
                                          email: email,
                                          password: "",
                                          password_confirmation: "" }
    assert_not flash.empty?
    assert_redirected_to @student
    @student.reload
    assert_equal @student.name, name
    assert_equal @student.email, email
  end

  test "successful edit with friendly forwarding" do
    get edit_student_path(@student)
    log_in_as(@student)
    assert_redirected_to edit_student_path(@student)
    name = "Foo Bar"
    email = "foo@bar.com"
    patch student_path(@student), student: { name: name,
                                            email: email,
                                            password: "foobar",
                                            password_confirmation: "foobar" }
    assert_not flash.empty?
    assert_redirected_to @student
    @student.reload
    assert_equal @student.name, name
    assert_equal @student.email, email
  end


end
