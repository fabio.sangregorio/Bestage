require 'test_helper'

class CompaniesSignupTest < ActionDispatch::IntegrationTest

  def setup
    ActionMailer::Base.deliveries.clear
  end

  test "invalid signup information" do
    get companies_signup_path
    assert_no_difference 'Company.count' do
      post companies_path, company: { name: "",
                                      email: "user@invalid",
                                      description: "",
                                      address: "",
                                      vacancies: -1,
                                      password: "foo",
                                      password_confirmation: "bar" }
    end
    assert_template 'companies/new'
  end

  test "email alredy taken by other model" do
    get companies_signup_path
    assert_no_difference "Company.count" do
      post_via_redirect companies_path,
                        company: {name: "example",
                                  email: schools(:marconi).email,
                                  description: "exxx",
                                  address: "ample",
                                  vacancies: 1,
                                  password: "foobar",
                                  password_confirmation: "foobar" }
    end
    assert_not flash.empty?
    assert_template "companies/new"
  end

  test "valid signup information with account activation" do
    get companies_signup_path
    assert_difference "Company.count", 1 do
      post companies_path, company: { name: "example",
                                      email: "user@valid.com",
                                      description: "exxx",
                                      address: "ample",
                                      vacancies: 1,
                                      password: "foobar",
                                      password_confirmation: "foobar"}
    end
    assert_equal 1, ActionMailer::Base.deliveries.size
    company = assigns(:company)
    assert_not company.activated?
    log_out
    # Try to log in before activation.
    log_in_as(company)
    assert_not is_logged_in?
    # Invalid activation token
    get edit_account_activation_path("invalid token")
    assert_not is_logged_in?
    # Valid token, wrong email
    get edit_account_activation_path(company.activation_token, email: 'wrong')
    assert_not is_logged_in?
    # Valid activation token
    get edit_account_activation_path(company.activation_token,
                                     email: company.email)
    assert company.reload.activated?
    follow_redirect!
    assert_template 'companies/show'
    assert is_logged_in?
  end

end
