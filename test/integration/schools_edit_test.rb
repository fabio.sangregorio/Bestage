require 'test_helper'

class SchoolsEditTest < ActionDispatch::IntegrationTest

  def setup
    @school = schools(:marconi)
  end

  test "unsuccessful edit" do
    log_in_as(@school)
    get edit_school_path(@school)
    assert_template "schools/edit"
    patch school_path(@school), school: { name: "",
                                          email: "foo@invalid",
                                          password: "foo",
                                          description: "fooo",
                                          password_confirmation: "foo" }
    assert_template "schools/edit"
  end

  test "successful edit" do
    log_in_as(@school)
    get edit_school_path(@school)
    assert_template "schools/edit"
    name = "Foo Bar"
    email = "foo@bar.com"
    patch school_path(@school), school: { name: name,
                                         email: email,
                                         password: "",
                                         password_confirmation: "" }
    assert_not flash.empty?
    assert_redirected_to @school
    @school.reload
    assert_equal @school.name, name
    assert_equal @school.email, email
  end

  test "successful edit with friendly forwarding" do
    get edit_school_path(@school)
    log_in_as(@school)
    assert_redirected_to edit_school_path(@school)
    name = "Foo Bar"
    email = "foo@bar.com"
    patch school_path(@school), school: { name: name,
                                         email: email,
                                         password: "",
                                         password_confirmation: "" }
    assert_not flash.empty?
    assert_redirected_to @school
    @school.reload
    assert_equal @school.name, name
    assert_equal @school.email, email
  end

end
