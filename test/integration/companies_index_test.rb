require 'test_helper'

class CompaniesIndexTest < ActionDispatch::IntegrationTest

  def setup
    @admin = schools(:marconi)
    @non_admin = companies(:abb)
  end

  test "index as admin including pagination and delete links" do
    log_in_as(@admin)
    get companies_path
    assert_template 'companies/index'
    assert_select 'div.pagination'
    first_page_of_companies = Company.paginate(page: 1)
    first_page_of_companies.each do |company|
      assert_select 'a[href=?]', company_path(company), text: company.name
      unless company == @admin
        assert_select 'a[href=?]', company_path(company), text: 'elimina',
        method: :delete
      end
    end
    assert_difference 'Company.count', -1 do
      delete company_path(@non_admin)
    end
  end

  test "index as non-admin" do
    log_in_as(@non_admin)
    get companies_path
    assert_select 'a', text: 'elimina', count: 0
  end

end
