require 'test_helper'

class CompaniesControllerTest < ActionController::TestCase

  def setup
    @company       = companies(:abb)
    @other_company = companies(:easytec)
    @school        = schools(:marconi)
  end

  test "should redirect index when not logged in" do
    get :index
    assert_redirected_to login_url
  end

  test "should redirect edit when not logged in" do
    get :edit, id: @company
    assert_not flash.empty?
    assert_redirected_to login_url
  end

  test "should redirect update when not logged in" do
    patch :update, id: @company, company: { name:  @company.name,
                                            email: @company.email }
    assert_not flash.empty?
    assert_redirected_to login_url
  end

  test "should redirect edit when logged in as wrong user" do
    log_in_as(@other_company)
    get :edit, id: @company
    assert flash.empty?
    assert_redirected_to root_url
  end

  test "should redirect update when logged in as wrong user" do
    log_in_as(@other_company)
    patch :update, id: @company, company: { name: @company.name,
                                            email: @company.email }
    assert flash.empty?
    assert_redirected_to root_url
  end

  test "should redirect edit and update when logged in as school" do
    log_in_as(@school)
    get :edit, id: @company
    assert flash.empty?
    assert_redirected_to root_url
    patch :update, id: @company, company: { name: "Foo Bar",
                                            email: @company.email }
    assert flash.empty?
    assert_equal @company.name, @company.reload.name
  end

  test "should redirect destroy when not logged in" do
    assert_no_difference 'Company.count' do
      delete :destroy, id: @company
    end
    assert_redirected_to login_url
  end

  test "should redirect destroy when logged in as a non-admin" do
    log_in_as(@company)
    assert_no_difference 'Company.count' do
      delete :destroy, id: @company
    end
    assert_redirected_to root_url
  end

end
