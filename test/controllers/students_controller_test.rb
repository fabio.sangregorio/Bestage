require 'test_helper'

class StudentsControllerTest < ActionController::TestCase
  def setup
    @student       = students(:archer)
    @other_student = students(:mary)
    @school        = schools(:marconi)
  end

  test "should redirect index when not logged in" do
    get :index
    assert_redirected_to login_url
  end

  test "should redirect new when not logged in" do
    get :new
    assert_redirected_to login_url
  end

  test "should redirect create when not logged in" do
    get :create
    assert_redirected_to login_url
  end

  test "should redirect edit when not logged in" do
    get :edit, id: @student
    assert_not flash.empty?
    assert_redirected_to login_url
  end

  test "should redirect update when not logged in" do
    patch :update, id: @student, student: { name:  @student.name,
                                            email: @student.email }
    assert_not flash.empty?
    assert_redirected_to login_url
  end

  test "should redirect edit when logged in as wrong user" do
    log_in_as(@other_student)
    get :edit, id: @student
    assert flash.empty?
    assert_redirected_to root_url
  end

  test "should redirect update when logged in as wrong user" do
    log_in_as(@other_student)
    patch :update, id: @student, student: { name: @student.name,
                                            email: @student.email }
    assert flash.empty?
    assert_redirected_to root_url
  end

  test "should not redirect edit and update when logged in as school" do
    log_in_as(@school)
    get :edit, id: @student
    assert flash.empty?
    assert_template "students/edit"
    patch :update, id: @student, student: { name: "Foo Bar",
                                            email: @student.email }
    assert_not flash.empty?
    assert_not_equal @student.name, @student.reload.name
  end

  test "should redirect destroy when not logged in" do
    assert_no_difference 'Student.count' do
      delete :destroy, id: @student
    end
    assert_redirected_to login_url
  end

  test "should redirect new and create when logged in as a non-admin" do
    log_in_as(@student)
    get :new
    assert_redirected_to root_url
    post :create, student: { name: "Foo Bar", email: @student.email }
    assert_redirected_to root_url
  end

  test "should redirect destroy when logged in as a non-admin" do
    log_in_as(@student)
    assert_no_difference 'Student.count' do
      delete :destroy, id: @student
    end
    assert_redirected_to root_url
  end

end
