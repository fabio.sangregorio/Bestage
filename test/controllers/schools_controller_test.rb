require 'test_helper'

class SchoolsControllerTest < ActionController::TestCase

  def setup
    @school       = schools(:marconi)
    @other_school = schools(:einaudi)
  end

  test "should redirect edit when not logged in" do
    get :edit, id: @school
    assert_not flash.empty?
    assert_redirected_to login_url
  end

  test "should redirect update when not logged in" do
    patch :update, id: @school, school: { name:  @school.name,
                                          email: @school.email }
    assert_not flash.empty?
    assert_redirected_to login_url
  end

  test "should redirect edit when logged in as wrong user" do
    log_in_as(@other_school)
    get :edit, id: @school
    assert flash.empty?
    assert_redirected_to root_url
  end

  test "should redirect update when logged in as wrong user" do
    log_in_as(@other_school)
    patch :update, id: @school, school: { name: @school.name,
                                          email: @school.email }
    assert flash.empty?
    assert_redirected_to root_url
  end

end
