require 'test_helper'

class SkillsControllerTest < ActionController::TestCase

  def setup
    @school = schools(:marconi)
    @student = students(:archer)
    @company = companies(:abb)
    @skill = skills(:sample)
  end

  test "should redirect index, new, create, edit, update when not logged in" do
    get :index
    assert_redirected_to login_url
    get :new
    assert_redirected_to login_url
    get :create
    assert_redirected_to login_url
    get :edit, id: @skill
    assert_redirected_to login_url
    patch :update, id: @skill, skill: { name: @skill.name,
                                        school_year: @skill.school_year }
    assert_redirected_to login_url
  end

  test "should redirect new, create, edit, update when logged as non-admin" do
    log_in_as(@student)
    get :new
    assert_redirected_to root_url
    get :create
    assert_redirected_to root_url
    get :edit, id: @skill
    assert_redirected_to root_url
    patch :update, id: @skill, skill: { name: @skill.name,
                                        school_year: @skill.school_year }
    assert_redirected_to root_url
  end


end
