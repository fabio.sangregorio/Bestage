class CreateStudents < ActiveRecord::Migration
  def change
    create_table :students do |t|
      t.string :name
      t.string :email
      t.date :birth_date
      t.integer :school_year

      t.timestamps null: false
    end
  end
end
