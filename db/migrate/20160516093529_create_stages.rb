class CreateStages < ActiveRecord::Migration
  def change
    create_table :stages do |t|
      t.integer :student_id
      t.integer :company_id
      t.date :start_time
      t.date :end_time

      t.timestamps null: false
    end
  end
end
