class CreateSchools < ActiveRecord::Migration
  def change
    create_table :schools do |t|
      t.string :name
      t.string :description
      t.string :address
      t.string :email

      t.timestamps null: false
    end
  end
end
