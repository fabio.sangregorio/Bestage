class CreateMatches < ActiveRecord::Migration
  def change
    create_table :matches do |t|
      t.integer :company_id
      t.integer :student_id
      t.float :variance

      t.timestamps null: false
    end
  end
end
