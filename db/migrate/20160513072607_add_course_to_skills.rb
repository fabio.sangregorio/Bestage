class AddCourseToSkills < ActiveRecord::Migration
  def change
    add_column :skills, :course, :string
  end
end
